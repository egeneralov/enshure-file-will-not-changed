package main

import (
  "os"
  "io"
	"fmt"
	"time"
  "flag"
  "bufio"
	"github.com/fsnotify/fsnotify"
)


var needRewrite bool = false


func fileExist(path string) (bool) {
  _, err := os.Stat(path)
  if err != nil {
    return false
  }
  return true
}


func main() {
  SrcFilePath := flag.String("src", "", "")
  DestFilePath := flag.String("dst", "", "")
  flag.Parse()

  if *SrcFilePath == "" {
    panic("--src is empty")
  }
  if *DestFilePath == "" {
    panic("--dst is empty")
  }

  for fileExist(*SrcFilePath) == false {
    panic("SrcFilePath " + *SrcFilePath + " not exist")
  }

  for fileExist(*DestFilePath) == false {
    fmt.Printf("DestFilePath '%s' not exist\n", *DestFilePath)
    time.Sleep(150 * time.Millisecond)
  }
  fmt.Printf("DestFilePath '%s' exist\n", *DestFilePath)
  needRewrite = true


  go func() {
    for {
      if needRewrite {
        fmt.Println("Gourutine RewriteFiles")
        RewriteFiles(*SrcFilePath, *DestFilePath)
        needRewrite = false
      }
      time.Sleep(100 * time.Millisecond)
    }
  }()
  
  
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
    panic(err)
	}
	defer watcher.Close()

	done := make(chan bool)

	go func() {
		for {
			select {
			case event := <-watcher.Events:
// 				fmt.Printf("EVENT %#v %#v\n", event.Op.String(), *DestFilePath)

        sig := event.Op.String()

        if sig == "WRITE" || sig == "CREATE" || sig == "CHMOD" {
  				fmt.Printf("EVENT %#v %#v\n", event.Op.String(), *DestFilePath)
          needRewrite = true
        }
        
			case err := <-watcher.Errors:
				fmt.Println("ERROR", err)
			}
		}
	}()

	if err := watcher.Add(*DestFilePath); err != nil {
		fmt.Println("ERROR", err)
//     panic(err)
	}

	<-done
}






func RewriteFiles(SrcFilePath string, DestFilePath string) {
  fi, err := os.Open(SrcFilePath)
  if err != nil {
    panic(err)
  }
  // close fi on exit and check for its returned error
  defer func() {
    if err := fi.Close(); err != nil {
      panic(err)
    }
  }()
  // make a read buffer
  r := bufio.NewReader(fi)

  // open output file
  fo, err := os.Create(DestFilePath)
  if err != nil {
    panic(err)
  }
  // close fo on exit and check for its returned error
  defer func() {
    if err := fo.Close(); err != nil {
      panic(err)
    }
  }()
  // make a write buffer
  w := bufio.NewWriter(fo)

  // make a buffer to keep chunks that are read
  buf := make([]byte, 1024)
  for {
    // read a chunk
    n, err := r.Read(buf)
    if err != nil && err != io.EOF {
      panic(err)
    }
    if n == 0 {
      break
    }

    // write a chunk
    if _, err := w.Write(buf[:n]); err != nil {
      panic(err)
    }
  }

  if err = w.Flush(); err != nil {
    panic(err)
  }
}

